const makeTea = (enoughWater, milk, lowFat, sugar) => {
  if (enoughWater) {
    console.log('Boil water')
    console.log('Add tea')

    console.log('milk?')
    if (milk) {
      console.log('low fat?')
      if (lowFat) {
        console.log('Add low fat milk')
      } else {
        console.log('Add normal milk')
      }
    }

    console.log('Sugar?')
    if(sugar) {
      console.log('Add sugar')
    }
    console.log('Stir')
    console.log('Wait 3 minutes')
    console.log('Please take your tea')
  } else {
    console.log('Please fill up water')
  }
}

module.exports = makeTea

const enoughWater = true;
const milk = true;
const lowFat = false;
const sugar = true;

makeTea(enoughWater, milk, lowFat, sugar);
