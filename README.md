# Laboratorio

## Instrucciones

* Calcule el número de escenarios posibles para cubrir al 100% de statement coverage (todos los caminos).
* Usando el segundo método visto en la clase anterior N + 1, calcule el cyclomatic complexity.

## Tabla de de decisiones:

| enoughWater | milk  | lowFat | sugar | RESULTADO                               |
|-------------|-------|--------|-------|-----------------------------------------|
| true        | flase | false  | false | enoughWater true and other values false |
| true        | true  | true   | true  | make tea with low fat milk and sugar    |
| true        | true  | true   | false | make tea with low fat milk and no sugar |
| true        | true  | false  | true  | make tea with regular milk and sugar    |
| true        | true  | false  | false | make tea with regular milk and no sugar |
| false       | false | false  | false | print "Please fill up with water".      |

## npm run coverage (jest --coverage) - statement coverage

| File      | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s |
|-----------|---------|----------|---------|---------|-------------------|
| main.js   |     100 |      100 |     100 |     100 |                   |

## Cyclomatic Complexity

* Fórmula: M = E - N + 2; E (aristas) = 7; N (Nodos) = 7
* M (cyclomatic complexity) = 2

