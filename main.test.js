const makeTea = require('./main')

describe('makeTea function', () => {
  test('make tea when enoughWater is true and other values are false', () => {
    makeTea(true, false, false, false)
  })

  test('make tea with low fat milk and sugar', () => {
    makeTea(true, true, true, true)
  })

  test('make tea with low fat milk and no sugar', () => {
    makeTea(true, true, true, false)
  })

  test('make tea with regular milk and sugar', () => {
    makeTea(true, true, false, true)
  })

  test('make tea with regular milk and no sugar', () => {
    makeTea(true, true, false, false)
  })

  test('print "Please fill up water" when enoughWater is false', () => {
    makeTea(false, true, true, true)
  })
})
